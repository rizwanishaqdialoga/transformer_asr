import tensorflow as tf

def feed_forward(inp, d_model, d_ff, scope='ff'):
      # Feed forward
  with tf.variable_scope(scope):
    out = tf.layers.conv1d(inp, filters=d_ff, kernel_size=1, activation=tf.nn.relu)
    out = tf.layers.conv1d(out, filters=d_model, kernel_size=1)
  return out

def encoder_block(input_e, d_model, num_heads,d_ff, is_training):
  
  with tf.variable_scope('mult-head-attn'):
    depth = tf.cast(d_model/num_heads, tf.int32)
    # Linear projection to d_model dimension: [batch, frame, d_model]
    Q = tf.layers.dense(input_e, d_model, activation=tf.nn.relu)
    K = tf.layers.dense(input_e, d_model, activation=tf.nn.relu)
    V = tf.layers.dense(input_e, d_model, activation=tf.nn.relu)
    
    # Split the matrix to multiple heads and then concatenate to have a larger batch size
    Q_split = tf.reshape(Q, [tf.shape(Q)[0],tf.shape(Q)[1], num_heads, depth])
    Q_split = tf.transpose(Q_split, [0,2,1,3])
    K_split = tf.reshape(K, [tf.shape(K)[0],tf.shape(K)[1], num_heads, depth])
    K_split = tf.transpose(K_split, [0,2,1,3])
    V_split = tf.reshape(V, [tf.shape(V)[0],tf.shape(V)[1], num_heads, depth])
    V_split = tf.transpose(V_split, [0,2,1,3])
    
    
   # Scaled_dot_product_attention
    out = tf.matmul(Q_split, K_split, transpose_b=True)   # [h*batch_size, frame, k_size]
    out = out / tf.sqrt(tf.cast(d_model/num_heads, tf.float32)) # scaled by sqrt(d_k)
    
    out = tf.nn.softmax(out)
    out = tf.layers.dropout(out, rate = 0.5, training=is_training)
    out = tf.matmul(out, V_split) # [h* batch, q_size, d_model]
    
     # Merge the multi-head back to original shape
    out = tf.transpose(out, [0,2,1,3])
    out = tf.reshape(out, [tf.shape(out)[0], tf.shape(out)[1], -1])
    
  # Add and Normalization
  with tf.variable_scope('Add_and_Normalization'):
    out = tf.contrib.layers.layer_norm(out + input_e, center=True, scale=True)
    
  # Feed Forward, Add and Normalization
  with tf.variable_scope('Feed_forward_and_Add_and_Normalization'):
    out = tf.contrib.layers.layer_norm(out + feed_forward(out, d_model, d_ff), center=True, scale=True)
      
  return out