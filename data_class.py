import os
from glob import glob
import tensorflow as tf
import functools
from scipy.signal import spectrogram

def data_input_fn(tfrecord, worker_index, num_workers, batch_size=10,n_epochs=1):
    #char2ind = tf.constant([" ","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"])
    #table = tf.contrib.lookup.index_table_from_tensor(mapping=char2ind,num_oov_buckets=1, default_value=-1)

    def _positional_encoding(spectrogram, labels, seq_lens):
        dim_embed = tf.shape(spectrogram)[1]
        pow_range = tf.range(0, dim_embed+1, 1)
    
        denoms = tf.expand_dims(tf.pow(10000.0, tf.div(tf.cast(2*pow_range, dtype=tf.float32), tf.cast(dim_embed, tf.float32))), 0)
        pos = tf.expand_dims(tf.range(0, seq_lens, 1), 1)

        # denoms works on last dimension and pos is over the time
        # dimension. broadcasting helps the row vector and column vector
        # create the correct arguments for sin and cos functions.
        sincos_args_base = tf.cast(pos, tf.float32)/denoms
        sargs = tf.sin(tf.transpose(tf.gather(tf.transpose(sincos_args_base), tf.range(0,dim_embed+1,2))))
        cargs = tf.cos(tf.transpose(tf.gather(tf.transpose(sincos_args_base), tf.range(1,dim_embed+1,2))))

 
        pos_encoding = tf.reshape(tf.concat([sargs[...,tf.newaxis], cargs[...,tf.newaxis]], axis=-1), [tf.shape(sargs)[0],-1])[:,:dim_embed]
        # The slice is taken considering for the case when dim_embed is odd.
        spectrogram = spectrogram + pos_encoding
        return spectrogram, labels, seq_lens


    def _spectrogram(audio,label, frame_length=200, frame_step=120, fft_length=1024, sample_rate=8000, log_offset=1e-12):
        ''' The parsing function which return the spectrogram and labels'''
        ''' The spectrogram is calculated with sampling frequency of 8000 Hz and frame_length of 25 msec with 15 msec frame_step'''
        '''' It is similar to scipy.spectrogram where frame_length=200(25 msec) and frame_step=80(10 msec) instead of 120'''

        stft = tf.contrib.signal.stft(audio,
                                    frame_length=frame_length,
                                    frame_step=frame_step,
                                    fft_length=fft_length,
                                    window_fn=tf.contrib.signal.hann_window)

        #stft = tf.py_func(_scipy_spectrogram, [audio], tf.float32)
        spectrogram = tf.div(tf.log(tf.abs(stft)+log_offset),tf.log(tf.constant(10.,dtype=tf.float32)))
        b_mean,b_variance = tf.nn.moments(spectrogram,axes=[-2])
        spectrogram = tf.nn.batch_normalization(x=spectrogram,mean=b_mean,variance=b_variance, offset=None, scale=None, variance_epsilon=1e-12)
        seq_lens = tf.shape(spectrogram)[-2]
        return spectrogram, tf.cast(label,tf.int32), seq_lens


    def _parse_final(spectrogram, label, seq_lens):
        features = {'spectrogram': spectrogram, 'seq_lens':seq_lens}
        return features, label


    def _parse_record(tf_record):
        features = {'audio': tf.FixedLenSequenceFeature([], tf.float32,allow_missing=True),
                            'labels': tf.FixedLenSequenceFeature([], dtype=tf.int64, allow_missing=True)}
        # Parse the serialized data so we get a dict with our data.
        parsed_example = tf.parse_single_example(serialized=tf_record, features=features)
        audio = parsed_example['audio']
        label = parsed_example['labels']

        return audio, label
    files = tf.data.Dataset.list_files(tfrecord)
    dataset = files.apply(tf.contrib.data.parallel_interleave(
                    lambda filename: tf.data.TFRecordDataset(filename,buffer_size=None, num_parallel_reads=8),
                    cycle_length=8,
                    sloppy=True))

    
    dataset = dataset.apply(tf.contrib.data.shuffle_and_repeat(batch_size, n_epochs))
    dataset = dataset.map(_parse_record, num_parallel_calls=8)
    dataset = dataset.map(_spectrogram, num_parallel_calls=8)
    dataset = dataset.map(_positional_encoding, num_parallel_calls=8)
    dataset = dataset.padded_batch(batch_size,([None,513],[None],[]), padding_values=(0., tf.constant(500,dtype=tf.int32), tf.constant(500,dtype=tf.int32)))
    dataset = dataset.map(_parse_final,num_parallel_calls=8)
    dataset = dataset.prefetch(batch_size)
    return dataset


class DataSet:
    def __init__(self, tfrecord, batch_size, n_epochs, num_workers, worker_index):
        self.tfrecord = os.path.join(tfrecord,'*.tfrecords')
        self.num_workers = num_workers
        self.worker_index = worker_index
        self.batch_size = batch_size
        self.n_epochs = n_epochs

    def __call__(self):
        return data_input_fn(self.tfrecord,self.worker_index, self.num_workers, batch_size=self.batch_size,n_epochs=self.n_epochs)

if __name__ == '__main__':
    pass
