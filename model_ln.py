# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import tensorflow as tf
from utils import CBHG
from transformer_network import encoder_block, feed_forward
from dictionary import dictiony_char_int

tf.logging.set_verbosity(tf.logging.INFO)


def network(features, mode, params):

    # Default parameters
    num_enc_layers = params['num_enc_layers']
    num_classes = params['num_classes']
    num_heads = params['num_heads']
    d_ff = params['d_ff']
    d_model = params['d_model']
    keep_prob = params['keep_prob']


    is_training = mode==tf.estimator.ModeKeys.TRAIN

    outputs = features['spectrogram']
    for i in range(num_enc_layers):
        with tf.variable_scope('enc_{}'.format(i)):
            outputs = encoder_block(outputs, d_model, num_heads, d_ff,  is_training=is_training)
        
    
     # 3rd Layer
    layer_3 = tf.layers.conv1d(outputs, filters=256, kernel_size=1, activation=tf.nn.relu)

    # Final_fully_connected_layer
    logits= tf.layers.dense(inputs=layer_3, units=num_classes,
                            kernel_initializer=tf.contrib.layers.xavier_initializer(),
                            activation=None)
    logits = tf.transpose(logits,[1,0,2])

    return logits



def optimizer_fn(cost, initial_learning_rate):
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    opt = tf.train.AdamOptimizer(initial_learning_rate)
    with tf.control_dependencies(update_ops):
        train_optimizer = opt.minimize(cost, global_step=tf.train.get_global_step())
  
    return train_optimizer

def loss_fn(logits, labels, seq_lens):
    loss = tf.nn.ctc_loss(labels, logits, seq_lens, ignore_longer_outputs_than_inputs=True)
    ctc_cost = tf.reduce_mean(loss)
    tf.losses.add_loss(ctc_cost)
    cost = tf.losses.get_total_loss()


    return cost

def config_fn():
    per_process_gpu_memory_fraction = 1.0 / 1.0
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=per_process_gpu_memory_fraction,
                                allow_growth=True)
    tf_config = tf.ConfigProto(gpu_options=gpu_options,
                               allow_soft_placement=True)
    config = tf.estimator.RunConfig(save_summary_steps=100,
                                    save_checkpoints_steps=100,
                                    session_config=tf_config,
                                    keep_checkpoint_every_n_hours=1,
                                    log_step_count_steps=100,
                                    keep_checkpoint_max=50)
    return config

def model_fn(features, labels, mode, params):
    logits = network(features, mode, params)
    #list_of_variable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    # Network
    decoded, log_prob = tf.nn.ctc_greedy_decoder(logits, sequence_length=features['seq_lens'])
    #decoded, log_prob = tf.nn.ctc_beam_search_decoder(logits, sequence_length=features['seq_lens'])
    # decoding
    decoded = tf.to_int32(decoded[0])
    preds = tf.sparse_tensor_to_dense(decoded, default_value=-1)
    predictions = {'preds': preds, 'logit':logits, 'shape':tf.shape(logits)}
    # Prediction

    if mode == tf.estimator.ModeKeys.PREDICT:
        export_outputs = {
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
            tf.estimator.export.PredictOutput(outputs=predictions)}
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs= export_outputs)
        #return tf.estimator.EstimatorSpec(mode=mode)

    labels = tf.contrib.layers.dense_to_sparse(labels,eos_token=500)
    loss = loss_fn(logits,labels,features['seq_lens'])
    tf.summary.scalar('loss', loss)
    edit_dist_op = tf.reduce_mean(tf.edit_distance(decoded, labels))
    tf.summary.scalar('edit_dist', edit_dist_op)
    lth = tf.train.LoggingTensorHook({'edit_dist': edit_dist_op}, every_n_iter=100)
    if mode == tf.estimator.ModeKeys.TRAIN:
        train_op = optimizer_fn(loss,params['learning_rate'])

        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, training_hooks=[lth])

    if mode == tf.estimator.ModeKeys.EVAL:
        eval_metric_ops = {'edit_dist': tf.metrics.mean(edit_dist_op)}
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops, evaluation_hooks=[lth])





def serving_input_receiver_fn():
    def _positional_encoding(spectrogram, seq_lens):
        dim_embed = tf.shape(spectrogram)[1]
        pow_range = tf.range(0, dim_embed+1, 1)
    
        denoms = tf.expand_dims(tf.pow(10000.0, tf.div(tf.cast(2*pow_range, dtype=tf.float32), tf.cast(dim_embed, tf.float32))), 0)
        pos = tf.expand_dims(tf.range(0, seq_lens, 1), 1)

        # denoms works on last dimension and pos is over the time
        # dimension. broadcasting helps the row vector and column vector
        # create the correct arguments for sin and cos functions.
        sincos_args_base = tf.cast(pos, tf.float32)/denoms
        sargs = tf.sin(tf.transpose(tf.gather(tf.transpose(sincos_args_base), tf.range(0,dim_embed+1,2))))
        cargs = tf.cos(tf.transpose(tf.gather(tf.transpose(sincos_args_base), tf.range(1,dim_embed+1,2))))

 
        pos_encoding = tf.reshape(tf.concat([sargs[...,tf.newaxis], cargs[...,tf.newaxis]], axis=-1), [tf.shape(sargs)[0],-1])[:,:dim_embed]
        # The slice is taken considering for the case when dim_embed is odd.
        spectrogram = spectrogram + pos_encoding
        return spectrogram, seq_lens
    def _spectrogram(reciever_tensors, frame_length=200, frame_step=120, fft_length=1024, sample_rate=8000, log_offset=1e-12):
        stft = tf.contrib.signal.stft(reciever_tensors['audio'],
                                  frame_length=frame_length,
                                  frame_step=frame_step,
                                  fft_length=fft_length,
                                  window_fn=tf.contrib.signal.hann_window)
        spectrogram = tf.div(tf.log(tf.abs(stft)+log_offset),tf.log(tf.constant(10.,dtype=tf.float32)))
        b_mean,b_variance = tf.nn.moments(spectrogram,axes=[-2])
        spectrogram = tf.nn.batch_normalization(x=spectrogram,mean=b_mean,variance=b_variance, offset=None, scale=None, variance_epsilon=1e-6)
        seq_lens = tf.shape(spectrogram)[-2][None]
        return spectrogram, seq_lens
    """
    This is used to define inputs to serve the model
    """
    receiver_tensors = {
        'audio': tf.placeholder(tf.float32, [None,None])
    }
    spectrogram, seq_lens = _spectrogram(receiver_tensors)
    spectrogram, _ =  _positional_encoding(spectrogram, seq_lens)
    features = {
        'spectrogram': spectrogram,
        'seq_lens': seq_lens
    }


    return tf.estimator.export.ServingInputReceiver(receiver_tensors=receiver_tensors,
                                                    features=features)




class Speech2Text():
    def __init__(self, num_enc_layers, num_heads, num_classes, num_features, learning_rate, d_ff):
        params = {
            ''
            'num_enc_layers': num_enc_layers,
            'num_classes': num_classes,
            'num_heads': num_heads,
            'd_model':num_features,
            'learning_rate':learning_rate,
            'keep_prob':0.95,
            'd_ff': d_ff
        }

        config = config_fn()
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='hdfs://hadoop-cluster-dialoga/users/rizwan/speech_2_text/model/',config=config)
        #latest_ckp = tf.train.latest_checkpoint('./warm_from')
        #reader = pywrap_tensorflow.NewCheckpointReader(latest_ckp)
        #var_to_shape_map = reader.get_variable_to_shape_map()
        #print(len(var_to_shape_map))
        #del(var_to_shape_map['fully_connected_1/weights'])
        #del(var_to_shape_map['fully_connected_1/biases/Adam'])
        #print(len(var_to_shape_map))
       # ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from", 
                                            #vars_to_warm_start='^(bidirectional_rnn.*|conv1d.*|prenet.*|highwaynet.*)')
        #ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from")
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config, warm_start_from= ws)
        self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config)


    def fit(self, train_input_fn):
        self.model.train(input_fn=train_input_fn)

    def evaluate(self, eval_input_fn):
        eval_results = self.model.evaluate(input_fn=eval_input_fn)
        return eval_results

    def train_and_evaluat(self,train_input_fn,eval_input_fn):
        train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn)
        eval_spec = tf.estimator.EvalSpec(input_fn=eval_input_fn,  start_delay_secs = 60*60)
        tf.estimator.train_and_evaluate(self.model, train_spec, eval_spec)

    def export_model(self):
        self.model.export_savedmodel('./pb', serving_input_receiver_fn=serving_input_receiver_fn)


    
